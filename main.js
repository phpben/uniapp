import App from './App'
import {
	createSSRApp
} from 'vue'
import store from './store'
import config from './config'
import cache from './common/utils/cache'
import tool from './common/utils/tool'
import http from './common/utils/request'
import api from './api'

export function createApp() {
	const app = createSSRApp(App)
	app.use(store)
	// 全局对象
	app.config.globalProperties.$CONFIG = config;
	app.config.globalProperties.$CACHE = cache;
	app.config.globalProperties.$TOOL = tool;
	app.config.globalProperties.$HTTP = http;
	app.config.globalProperties.$API = api;
	// 全局组件
	//app.component('test', test);

	return {
		app
	}
}

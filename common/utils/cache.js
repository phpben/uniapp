export default {
	set(key, value, expires) {
		let options = {
			name: key,
			value: value,
			expires: expires ? Date.parse(new Date()) + expires : null,
		}
		uni.setStorageSync(options.name, options);
	},
	get(name) {
		let item = uni.getStorageSync(name);
		if (!item) {
			return false;
		}
		if (item.expires === null) {
			return item.value;
		} else {
			let timestamp = Date.parse(new Date());
			if (timestamp >= item.expire) {
				uni.removeStorageSync(name)
				return false;
			} else {
				return item.value;
			}
		}
	},
	remove(name) {
		uni.removeStorageSync(name)
	},
	clear() {
		uni.clearStorageSync()
	}
};

import store from "@/store"
import config from "@/config"

// 拦截器
uni.addInterceptor('request', {
	invoke(args) {
		let {
			token
		} = store.state.userinfo;
		if (token) {
			args.headers[config.TOKEN_NAME] = config.TOKEN_PREFIX + token
		}
		if (!args.headers) {
			args.headers = {};
		}
		if (!config.REQUEST_CACHE && (config.method == 'get' || config.method == 'GET')) {
			args.data = args.data || {};
			args.data['_'] = new Date().getTime();
		}
		Object.assign(args.headers, config.HEADERS)
		args.url = config.API_URL + args.url
		return args;
	},
	success(args) {
		if (args.data.code === 401) {
			return Promise.reject(args.data)
		}
	},
	fail(err) {
		return Promise.reject(err)
	}
})

// 请求
let http = {
	get: function(url, params = {}, config = {}) {
		return new Promise((resolve, reject) => {
			uni.request({
				url: url,
				data: params,
				method: 'GET',
				success: function(response) {
					resolve(response.data);
				},
				fail: function(error) {
					reject(error);
				},
				...config
			})
		})
	},
	post: function(url, params = {}, config = {}) {
		return new Promise((resolve, reject) => {
			uni.request({
				url: url,
				data: params,
				method: 'POST',
				success: function(response) {
					resolve(response.data);
				},
				fail: function(error) {
					reject(error);
				},
				...config
			})
		})
	},
};

export default http;

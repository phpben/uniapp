export default {
	groupSeparator: function(num) {
		num = num + '';
		if (!num.includes('.')) {
			num += '.'
		}
		return num.replace(/(\d)(?=(\d{3})+\.)/g, function($0, $1) {
			return $1 + ',';
		}).replace(/\.$/, '');
	},
	datetime: function(value) {
		if (value == null || value == "") return "无";
		value = parseInt(value);
		if (value.toString().length == 10) {
			value = value * 1000;
		}
		let date = new Date(value);
		let Y = date.getFullYear() + '-';
		let M = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) + '-' : date.getMonth() + 1 + '-';
		let D = date.getDate() < 10 ? '0' + date.getDate() + ' ' : date.getDate() + ' ';
		let h = date.getHours() < 10 ? '0' + date.getHours() + ':' : date.getHours() + ':';
		let m = date.getMinutes() < 10 ? '0' + date.getMinutes() + ':' : date.getMinutes() + ':';
		let s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
		return Y + M + D + h + m + s;
	},
}

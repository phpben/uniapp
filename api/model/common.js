import http from "@/common/utils/request"

export default {
	upload: function(params = {}) {
		let url = '/api/uploads';
		return http.post(url, params);
	},
}

import {
	createStore
} from 'vuex';
import cache from '@/common/utils/cache'

const state = {
	userinfo: {}
};

const mutations = {
	setUser(state, userinfo) {
		cache.set('USERINFO', userinfo)
		state.userinfo = userinfo
	},
	initUser(state) {
		let userinfo = cache.get("USERINFO");
		if (userinfo) {
			state.userinfo = userinfo
		}
	}
};

export default createStore({
	state: state,
	mutations: mutations
});

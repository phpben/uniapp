export default {
	// 接口地址
	API_URL: "http://asd.com/",
	// TokenName
	TOKEN_NAME: "Authorization",
	// Token前缀，注意最后有个空格，如不需要需设置空字符串
	TOKEN_PREFIX: "Bearer ",
	// 追加其他头
	HEADERS: {},
	// GET缓存
	REQUEST_CACHE: false,
};
